#logger_config.py
import logging
from logging.handlers import TimedRotatingFileHandler
import os
from config import LOG_DIARIO, LOGNAME, TOKEN_LOGNAME

def setup_logger():
    if not os.path.exists(LOG_DIARIO):
        os.makedirs(LOG_DIARIO)

    log_path = os.path.join(LOG_DIARIO, LOGNAME)
    token_log_path = os.path.join(LOG_DIARIO, TOKEN_LOGNAME)
    format = '[%(levelname)7s] [%(asctime)s] [%(threadName)s:%(thread)7d] [line:%(lineno)4d] %(message)s'
    datefmt = '%Y/%m/%d %I:%M:%S %p'
    level = logging.INFO

    # Main logger
    file_handler = TimedRotatingFileHandler(log_path, when="M", interval=5, backupCount=60, encoding='utf-8')
    file_handler.setLevel(level)
    file_handler.setFormatter(logging.Formatter(format, datefmt))

    logger = logging.getLogger(__name__)
    logger.setLevel(level)
    logger.addHandler(file_handler)

    # Token logger
    token_format = '[%(asctime)s] %(message)s'
    token_handler = TimedRotatingFileHandler(token_log_path, when="D", interval=1, backupCount=7, encoding='utf-8')
    token_handler.setLevel(level)
    token_handler.setFormatter(logging.Formatter('%(message)s'))  # Simple format for tokens

    token_logger = logging.getLogger('token_logger')
    token_logger.setLevel(level)
    token_logger.addHandler(token_handler)

    return logger, token_logger

logger, token_logger = setup_logger()
message = "---- INICIO DEL PROGRAMA ----"
logger.info(message)
print(message)
