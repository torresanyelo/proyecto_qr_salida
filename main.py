# main.py
import time
import evdev
from evdev import InputDevice, categorize, ecodes, list_devices
from threading import Thread
from config import DEVICE_NAME, QR_EXPIRATION_TIME
from logger_config import logger
from utils import peticion_back

def find_device_by_name(name):
    devices = [InputDevice(path) for path in list_devices()]
    for device in devices:
        if device.name == name:
            return device.path
    return None

def lectura_QR(device_path):
    device_connected = False
    last_processed_qr = ""
    last_processed_time = 0

    while True:
        try:
            dev = InputDevice(device_path)
            dev.grab()
            message = "Dispositivo conectado"
            logger.info(message)
            print(message)
            device_connected = True

            scancodes = {
                0: None, 1: 'ESC', 2: '1', 3: '2', 4: '3', 5: '4', 6: '5', 7: '6', 8: '7', 9: '8',
                10: '9', 11: '0', 12: '-', 13: '=', 14: 'BKSP', 15: 'TAB', 16: 'q', 17: 'w', 18: 'e', 19: 'r',
                20: 't', 21: 'y', 22: 'u', 23: 'i', 24: 'o', 25: 'p', 26: '[', 27: ']', 28: 'CRLF', 29: 'LCTRL',
                30: 'a', 31: 's', 32: 'd', 33: 'f', 34: 'g', 35: 'h', 36: 'j', 37: 'k', 38: 'l', 39: ';',
                40: '"', 41: '`', 42: 'LSHFT', 43: '\\', 44: 'z', 45: 'x', 46: 'c', 47: 'v', 48: 'b', 49: 'n',
                50: 'm', 51: ',', 52: '.', 53: '/', 54: 'RSHFT', 56: 'LALT', 57: ' ', 100: 'RALT'
            }

            capscodes = {
                0: None, 1: 'ESC', 2: '!', 3: '@', 4: '#', 5: '$', 6: '%', 7: '^', 8: '&', 9: '*',
                10: '(', 11: ')', 12: '_', 13: '+', 14: 'BKSP', 15: 'TAB', 16: 'Q', 17: 'W', 18: 'E', 19: 'R',
                20: 'T', 21: 'Y', 22: 'U', 23: 'I', 24: 'O', 25: 'P', 26: '{', 27: '}', 28: 'CRLF', 29: 'LCTRL',
                30: 'A', 31: 'S', 32: 'D', 33: 'F', 34: 'G', 35: 'H', 36: 'J', 37: 'K', 38: 'L', 39: ':',
                40: '\'', 41: '~', 42: 'LSHFT', 43: '|', 44: 'Z', 45: 'X', 46: 'C', 47: 'V', 48: 'B', 49: 'N',
                50: 'M', 51: '<', 52: '>', 53: '?', 54: 'RSHFT', 56: 'LALT', 57: ' ', 100: 'RALT'
            }

            y = ''
            caps = False

            for event in dev.read_loop():
                if event.type == ecodes.EV_KEY:
                    data = categorize(event)
                    if data.scancode == 42:
                        caps = data.keystate == 1
                    if data.keystate == 1:
                        key_lookup = capscodes.get(data.scancode) if caps else scancodes.get(data.scancode)
                        if key_lookup and data.scancode not in [42, 28]:
                            y += key_lookup
                        if data.scancode == 28:
                            cad = y
                            current_time = time.time()
                            if cad != last_processed_qr or (current_time - last_processed_time) > QR_EXPIRATION_TIME:
                                last_processed_qr = cad
                                last_processed_time = current_time
                                thread2 = Thread(target=peticion_back, args=(cad,), name="Peticion back")
                                thread2.start()
                                message = f">>Hilo thread2 : {thread2}"
                                #logger.warning(message)
                                print(message)
                            y = ''
        except OSError as e:
            if device_connected:
                message = "Error conexion dispositivo"
                logger.error(f"{message}: {str(e)}")
                print(message)
                device_connected = False
        finally:
            try:
                dev.ungrab()
            except Exception as e:
                logger.error(f"Error al liberar el dispositivo: {e}")

device_path = find_device_by_name(DEVICE_NAME)
if device_path:
    thread1 = Thread(target=lectura_QR, args=(device_path,))
    thread1.start()
else:
    message = f"No se encontro el dispositivo con el nombre {DEVICE_NAME}"
    logger.error(message)
    print(message)