import socket
import os
import time
from datetime import datetime, timedelta
from config import LOG_DIARIO, TOKEN_LOGNAME

# Configuración del servidor
PORT = 65432      # Puerto a utilizar
LOG_FILE = os.path.join(LOG_DIARIO, TOKEN_LOGNAME)  # Nombre del archivo de registro

# Función para obtener la IP local de la Raspberry Pi
def get_local_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # Usamos una conexión ficticia para obtener la IP local
        s.connect(("8.8.8.8", 80))
        ip = s.getsockname()[0]
    except Exception:
        ip = None  # En caso de error, no obtener IP
    finally:
        s.close()
    return ip

# Función para verificar y eliminar el archivo log si tiene más de 7 días
def check_and_delete_log_file():
    if os.path.exists(LOG_FILE):
        file_creation_time = os.path.getctime(LOG_FILE)
        file_creation_date = datetime.fromtimestamp(file_creation_time)
        if datetime.now() - file_creation_date > timedelta(days=7):
            os.remove(LOG_FILE)
            print(f"{LOG_FILE} eliminado por superar los 7 días")

# Función para obtener la IP local de forma continua hasta que lo logre
def get_ip_continuously():
    while True:
        ip = get_local_ip()
        if ip:
            return ip
        else:
            if not os.path.exists(LOG_DIARIO):
                os.makedirs(LOG_DIARIO)
            with open(LOG_FILE, 'a') as log_file:
                log_file.write(f"{datetime.now()} - Error: No se pudo obtener la IP local. Reintentando...\n")
            print("Error: No se pudo obtener la IP local. Reintentando...")
            time.sleep(5)  # Esperar 5 segundos antes de reintentar

# Crear el directorio de logs si no existe
if not os.path.exists(LOG_DIARIO):
    os.makedirs(LOG_DIARIO)

# Función para verificar si el token ya está registrado
def token_ya_registrado(token):
    with open(LOG_FILE, 'r', encoding='utf-8') as f:
        for line in f:
            if token in line:
                return True
    return False

# Obtener la IP local de la Raspberry Pi de forma continua
HOST = get_ip_continuously()

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.listen()
    print(f"Servidor escuchando en {HOST}:{PORT}")
    
    check_and_delete_log_file()  # Verificar y eliminar archivo log si es necesario

    while True:
        conn, addr = s.accept()
        with conn:
            print(f"Conexión establecida con {addr}")
            while True:
                data = conn.recv(1024)
                if not data:
                    break
                token = data.decode('utf-8').strip()
                print(f"Recibido: {token}")
                
                # Verificar si el token ya está registrado
                if token_ya_registrado(token):
                    message = "Token ya registrado."
                    print(message)
                    conn.sendall(message.encode('utf-8'))
                else:
                    # Registrar datos en el archivo log con fecha y hora
                    with open(LOG_FILE, 'a') as log_file:
                        log_file.write(f"{datetime.now()} - {addr} - {token}\n")
                    
                    message = "Token registrado correctamente."
                    print(message)
                    conn.sendall(message.encode('utf-8'))
